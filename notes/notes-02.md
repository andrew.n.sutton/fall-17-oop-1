# Types and safety

The type system of a programming language is the set of types it uses to
a) describe values and b) determine when operations on those values are valid.
Every language has a set of built-in types (e.g., `int`, `bool`), and most
have ways to define new types. C++ has three ways of defining new types:
enumerations, classes, and unions.

A *type error* is kind of bug that results from using a value in a way other
than its original intent, typically by misinterpreting its representation.
For example, early languages did not differentiate between integer and floating
point values (bits are bits). In such languages it was fairly easy to 
misinterpret a floating point value as an integer, with seemingly random bits.

Other kinds of type errors are more subtle. For example, certain conversions
may lose information and cause bugs that way. The Ariane 5 crashed in 1996
as a result of converting a 64-bit double to a 16-bit integer, essentially
truncating the value. Automatically or incorrectly narrowing one set of values
to another is a common source of computation bugs.

Most modern approaches to programming involve the application of the type system
to eliminate or reduce type errors. This property is known as *type safety*.
Type safety is both a property of a programming language and software written
in the language. 

A type safe programming language makes it impossible to misinterpret values of 
one type as another. Most languages try to make it difficult to do that, but 
not impossible. Although this class doesn't cover type systems in depth, we
will talk about common notions of subtyping and value conversion in C++. These
ideas appear in most other languages as well.

Type safe software uses the language's type system to create new data types
to that are difficult to misuse. The goal in designing these data types is to
use the language's type rules to enforce safe usage of those abstractions.
User-defined types, notably enums and classes, can be used to improve
type safety in programs. A primary focus of this course is for you to learn
to use these language features to build type safe abstractions.

# Enumerations

Many properties of a concrete entity (e.g., a playing card) are best described
by a list of values. For example a card has both a rank and a suit. Both rank
and suit can be described by their name, but it's not otherwise clear how you
might represent them within a program. A typical solution is to assign an
integer value to each value of rank or suit (e.g., ace = 1, two = 2, etc.).

These kinds of properties are sometimes referred to as "nominal" values,
especially in statistics. The term "nominal" relates to the importance of the
value's name, not its corresponding value.

However, simply using `int`s to store these values leads to problems. See the
discussion in notes-1.md.

We should be creating a new data type to enumerate these small sets of values.
In particular, we can create an `enum`. For example:

    enum Rank {
      Ace, Two, Three, ..., Jack, Queen King,
    };

    enum Suit {
      Hearts, Diamonds, Clubs, Spades
    };

These declarations define two new enumeration data types: `Rank` and `Suit`. 
The values of these types are the names, called *enumerators*, contained within 
the braces.

Enumeration data types are essentially restricted forms of integer data types.
Each enumerator is automatically assigned a corresponding integer value, 
starting (by default) at 0 and increasing with each subsequent value. 

Because `enum`s are like integers, we can generally use them just like integer 
values. For example, we can do the following:

    Rank r1 = Ace; // Declare variables
    std::cout << (r1 == King) << '\n'; // Compare values
    std::cout << (Ace < Two) << '\n'; // Order values

However, we can't do everything that an `int` can do.

    std::cout << Ace + King << '\n'; // ???

# Conversions

A conversion is a function that transforms a value of one type (called the 
source value and type) into a value of a different type (called the destination 
type). Conversions are a major source of type error because the conversion may 
lose information. This happens when the destination type may not be able to
represent all of the values of the source type.

- Think of types as sets of values
- Think about Venn diagrams


A narrowing conversion is...


A widening conversion is...


An explicit conversion is a conversion requested by a programmer, often used
to perform a narrowing conversion. Explicit conversion are a way of acknowledge 
that the source value is known to representable in the destination type. That
is, the programmer is assuming responsibility for a potentially lossy 
conversion.

An implicit conversion is one that the programming language applies in certain
contexts to perform a widening conversion.


# Classes

Classes are the primary abstraction mechanism in all object-oriented programming
languages. Classes are used to create new aggregate data types. 
(Aggregate roughly "composed of other data types").

The goal is to defined data types that *represent* (or encode) abstract or
concrete entities (i.e., values).

TODO: Do a better job defining these terms.

- value -- an abstract or concrete entity
- representation -- the data types used to implement 
- object -- memory that holds a value
- state -- the values of an object's representation
- interpretation -- the value corresponding to an object's state


Our first attempt at defining a `Card` class looks like this:

    struct Card
    {
        Rank rank;
        Suit suit;
    };

A `struct` is a class; we could also have used the keyword `class`, but that's
for later.

This definition allows anybody to modify the member data of the class. This is 
because the members of the class are *publicly accessible* (members of a
`struct` are public by default). By making the members public, we haven't
created any strong guarantees that other programmers should not modify those
values. This could potentially lead to unintentional modification of a card's
state, resulting in bugs later on.

## Constructors

A constructor is used to (among other things) provide initial values for
an object.


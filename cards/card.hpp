// (c) 2017 Andrew Sutton, PhD
// All rights reserved

// Guarantee that this file is included exactly
// one time in any translation unit.
#pragma once

#include <iosfwd>
#include <utility>

enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King
};

enum Suit 
{
  Spades,
  Clubs,
  Hearts,
  Diamonds,
};

enum Color
{
  Black,
  Red,
};

/// Represents the value of a (suited) playing card.
struct SuitedCard
{
  Rank rank;
  Suit suit;
};

/// Represents a joker (of some color).
struct JokerCard
{
  Color color;
};


// Discriminator value.
enum CardKind
{
  Joker,
  Suited
};

// Union type.
union CardImpl
{
  CardImpl(Rank r, Suit s) : sc{r, s} { }
  CardImpl(Color c) : jc{c} { }
  
  SuitedCard sc;
  JokerCard jc;
};

// Discriminated union.
struct Card
{
  Card() = default;
  
  Card(Rank r, Suit s) : kind(Suited), impl(r, s) { }
  
  CardKind kind;
  CardImpl impl;
};


// Equality comparison
bool operator==(Card a, Card b);
bool operator!=(Card a, Card b);

// Ordering
bool operator<(Card a, Card b);
bool operator>(Card a, Card b);
bool operator<=(Card a, Card b);
bool operator>=(Card a, Card b);


// Declare the operator here.
// Only need to include <iosfwd>
std::ostream& operator<<(std::ostream& os, Card c);
std::ostream& operator<<(std::ostream& os, Rank r);
std::ostream& operator<<(std::ostream& os, Suit r);



// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#include "card.hpp"

// #include <cassert>
#include <iostream>
#include <queue>
#include <stack>

using Deck = std::vector<Card>;

using Hand = std::queue<Card>;

// Deal one card from the deck d to the hand h.
// The deck shall not be empty.
//
// This function has a contract:
//
// pre-condition: d.size() != 0 --- !d.empty()
void
deal_one(Deck& d, Hand& h)
{
  // Prefer this.
  assert(!d.empty());

  // Then this:
  if (d.empty())
    throw std::runtime_error("empty deck");

  // NEVER DO THIS. Silently fail.
  if (d.empty()) {
    std::cerr << "empty\n";
    return;
  }

  Card c = d.back();
  d.pop_back();
  h.push(c);
}

void 
print(const Deck &deck) 
{
  int i = 1;
  for (Card c : deck) {
    std::cout << c << ' ';
    if (i % 13 == 0) {
      std::cout << '\n';
      i = 0;
    }
    ++i;
  }
  std::cout << '\n';
}
int 
main()
{
  // CardImpl c;
  // c.sc = SuitedCard{Four, Hearts}; // sc is the active member
  
  // JokerCard jc = c.jc;
  // std::cout << jc.color << '\n';

  Deck d {
    {Ace, Spades},
    {Two, Spades},
    {Three, Spades},
    {Four, Spades},
    {Five, Spades},
    {Six, Spades},
    {Seven, Spades},
    {Eight, Spades},
    {Nine, Spades},
    {Ten, Spades},
    {Jack, Spades},
    {Queen, Spades},
    {King, Spades},

    {Ace, Clubs},
    {Two, Clubs},
    {Three, Clubs},
    {Four, Clubs},
    {Five, Clubs},
    {Six, Clubs},
    {Seven, Clubs},
    {Eight, Clubs},
    {Nine, Clubs},
    {Ten, Clubs},
    {Jack, Clubs},
    {Queen, Clubs},
    {King, Clubs},

    {Ace, Hearts},
    {Two, Hearts},
    {Three, Hearts},
    {Four, Hearts},
    {Five, Hearts},
    {Six, Hearts},
    {Seven, Hearts},
    {Eight, Hearts},
    {Nine, Hearts},
    {Ten, Hearts},
    {Jack, Hearts},
    {Queen, Hearts},
    {King, Hearts},

    {Ace, Diamonds},
    {Two, Diamonds},
    {Three, Diamonds},
    {Four, Diamonds},
    {Five, Diamonds},
    {Six, Diamonds},
    {Seven, Diamonds},
    {Eight, Diamonds},
    {Nine, Diamonds},
    {Ten, Diamonds},
    {Jack, Diamonds},
    {Queen, Diamonds},
    {King, Diamonds},
  };

  print(d);

  Deck d2;

  Hand h;

  // Called out of contract -- this is a bug. And
  // it's the caller's fault.
  deal_one(d2, h);

}



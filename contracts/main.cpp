// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#include "card.hpp"

#include <cassert>
#include <iostream>
#include <queue>
#include <stack>


using Deck = std::vector<Card>;

using Hand = std::queue<Card>;

// Deal one card from the deck d to the hand h.
//
// pre-condition: !d.empty()
//
// narrow contract: behavior is only defined for valid arguments/state. 
// leave behavior undefined for invalid arguments.
//
// wide contract: behavior is defined for all (?) arguments/program
// states. Some arguments result in error values or exceptions.
//
// postcondition: If d has (d1, d2, ..., dn-1, dn) and h has
// the value (h1, h2, ..., hm), after completion, d has the
// value (d1, d2, ..., dn-1) and h has the value
// (h1, h2, ..., hm, dn).
void
deal_one(Deck& d, Hand& h)
{
  assert(!d.empty());
#ifdef NDEBUG
  Card c = d.back();
  d.pop_back();
  h.push(c);
#else
  // Deck d0 = d;
  // Hand h0 = h;
  // Card c = d.back();
  // d.pop_back();
  // h.push(c);
  // assert(d.size() == d0.size() - 1);
  // assert(std::equal(d.begin(), d.end(), d0.begin()));
  // assert(h.size() == h0.size() + 1);
  // assert(std::equal(h0.begin(), h0.end(), h.begin()));
  // assert(h.back() == d0.back());
#endif
}



// void 
// print(const Deck &deck) 
// {
//   int i = 1;
//   for (Card c : deck) {
//     std::cout << c << ' ';
//     if (i % 13 == 0) {
//       std::cout << '\n';
//       i = 0;
//     }
//     ++i;
//   }
//   std::cout << '\n';
// }

int 
main()
{

  Card c(-5, 38);
  std::cout << c.rank << ' ' << c.suit << '\n';

  // // CardImpl c;
  // // c.sc = SuitedCard{Four, Hearts}; // sc is the active member
  
  // // JokerCard jc = c.jc;
  // // std::cout << jc.color << '\n';

  // Deck d {
  //   {Ace, Spades},
  //   {Two, Spades},
  //   {Three, Spades},
  //   {Four, Spades},
  //   {Five, Spades},
  //   {Six, Spades},
  //   {Seven, Spades},
  //   {Eight, Spades},
  //   {Nine, Spades},
  //   {Ten, Spades},
  //   {Jack, Spades},
  //   {Queen, Spades},
  //   {King, Spades},

  //   {Ace, Clubs},
  //   {Two, Clubs},
  //   {Three, Clubs},
  //   {Four, Clubs},
  //   {Five, Clubs},
  //   {Six, Clubs},
  //   {Seven, Clubs},
  //   {Eight, Clubs},
  //   {Nine, Clubs},
  //   {Ten, Clubs},
  //   {Jack, Clubs},
  //   {Queen, Clubs},
  //   {King, Clubs},

  //   {Ace, Hearts},
  //   {Two, Hearts},
  //   {Three, Hearts},
  //   {Four, Hearts},
  //   {Five, Hearts},
  //   {Six, Hearts},
  //   {Seven, Hearts},
  //   {Eight, Hearts},
  //   {Nine, Hearts},
  //   {Ten, Hearts},
  //   {Jack, Hearts},
  //   {Queen, Hearts},
  //   {King, Hearts},

  //   {Ace, Diamonds},
  //   {Two, Diamonds},
  //   {Three, Diamonds},
  //   {Four, Diamonds},
  //   {Five, Diamonds},
  //   {Six, Diamonds},
  //   {Seven, Diamonds},
  //   {Eight, Diamonds},
  //   {Nine, Diamonds},
  //   {Ten, Diamonds},
  //   {Jack, Diamonds},
  //   {Queen, Diamonds},
  //   {King, Diamonds},
  // };

  // // print(d);

  // Deck d2;

  // Hand h;

  // // Called out of contract -- this is a bug. And
  // // it's the caller's fault.
  // deal_one(d2, h);

  // // try {
  // //   deal_one(d2, h);
  // // } catch (std::logic_error& e) {

  // // }

}



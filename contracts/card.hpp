// (c) 2017 Andrew Sutton, PhD
// All rights reserved

// Guarantee that this file is included exactly
// one time in any translation unit.
#pragma once

#include <cassert>
#include <iosfwd>
#include <utility>

enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King
};

enum Suit 
{
  Spades,
  Clubs,
  Hearts,
  Diamonds,
};

enum Color
{
  Black,
  Red,
};


// Represents playing cards as a pair of rank and suit. Internally,
// these are encoded in an 8-bit byte with the following representation:
//
//
//    0b00ssrrrr
//
// Where ss are the bits needed to encode the suit value and rrrr are the
// bits needed to encode the rank.
struct Card
{
  Card()
    : val((Spades << 4) | Ace)
  { }

  Card(const Card& c) 
    : val(c.val)
  { }

  // precondition: Ace <= r && r <= King
  // precondition: Spades <= s && s <= Diamonds
  Card(int r, int s)
    : val((r << 4) | s)
  {
    assert(Ace <= r && r <= King);
    assert(Spades <= s && s <= Diamonds);
  }

  // Uses the type system to enforce logical properties
  Card(Rank r, Suit s)
    : rank(r), suit(s)
  { }


  unsigned char val;
};


Card c;
Card c2 = c;
Card c3 {Ace, Spades};

Card c4((Rank)100, (Suit)100);

Card make_card(int r, int s)
{
  if(Ace <= r && r <= King)
    if(Spades <= s && s <= Diamonds)
      return Card(r, s);
  throw std::runtime_error("invalid card");
}


std::istream& 
operator<<(std::istream& is, Card& c)
{
  int r, int s;
  is << r << s;
  if(Ace <= r && r <= King)
    if(Spades <= s && s <= Diamonds) {
      c = Card(r, s);
      return is;
    }
   is.setstate(std::ios::failbit);
   return is;
}


Card c5 = make_card(4, 3);
// Card c6 = make_card(4000, 3000);






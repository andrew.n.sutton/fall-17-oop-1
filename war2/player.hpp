// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#pragma once

#include "queue.hpp"

struct Player
{
  CardQueue hand;
};

// (c) 2017 Andrew Sutton, PhD
// All rights reserved

// #include "game.hpp"
#include "deck.hpp"

#include <iostream>
#include <vector>

#include <random>

std::random_device rng;

// Global variable.
std::minstd_rand prng;


// Deck of cards
//  - which cards? standard/suited cards.
//  - jokers?
//  - how many of each card
//    - 1 deck?
//    - 2 decks?

// 2 players (or more? ;)
//  - each player has a hand (or stack of cards)
//  - other properties
//    - no AI, no strategies
//    - name? p1 p2 are good.

// spoils pile
//  - order of acquisition
//      - random
//      - winner on top
//      - loser on top
//      - interspersed piles?

// game options
//  - selection of cards (jokers?)
//  - number decks
//  - ace high/low
//  - number sacrificial cards
//  - negotiable sacrifice
//  - acquisition method


int 
main()
{
  // Seed the random number generator.
  prng.seed(rng());

  Game g;


  Deck d1 = make_standard_deck();
  Deck d2 = make_standard_deck();
  Deck d3 = make_deck(d1, d2);
  shuffle(d3);
  print(d3);



  // constexpr int runs = 10'000;
  // int turns = 0;
  // for (int i = 0; i < runs; ++i) {
  //   Game g;
  //   turns += g.play();
  // }

  // std::cout << "average: " << float(turns) / float(runs) << '\n';





#if 0


  Player p1;
  Player p2;

  Deck deck;

  deck.shuffle();

  // See below.
  // deal(deck, p1, deck.size() / 2);
  // deal(deck, p2, deck.size());

  // I like this...
  deal(deck, {p1, p2});
  
  // We could do this...
  //
  // But this would "cobble" decks and players together and
  // make them a little less re-usable.
  // deck.deal(p1, deck.size() / 2);
  // deck.deal(p2, deck);

  // Randomized data structure?
  Pile pile;

  while (true) {
    if (p1.empty()) {
      if (p2.empty())
        std::cout < "TIE\n";
      else
        std::cout << "P2 WINS\n";
      break;
    }
    else if (p2.empty())
      std::cout << "P1 WINS\n";
      break;
    }

    Card c1 = p1.take();
    Card c2 = p2.take();

    // Does this respect acquisition order?
    if (c1 > c2) {
      pile.add(c1);
      pile.add(c2);
      give(p1, pile);
    }
    else if (c2 > c1) {
      pile.add(c2);
      pile.add(c1);
      give(p2, pile);
    }
    else {

      // This condition depends on the number of sacrifices.
      if (p1.empty()) {
        if (p2.empty())
          std::cout < "TIE\n";
        else
          std::cout << "P2 WINS\n";
        break;
      }
      else if (p2.empty())
        std::cout << "P1 WINS\n";
        break;
      }


      // Number of sacrifices?
      Card s1 = p1.take();
      Card s2 = p2.take();
      pile.add(s1);
      pile.add(s2);
      continue;
    }


    assert(pile.empty());
  }


  // Initialize a deck of cards.
  std::vector<Card> deck;
  deck.reserve(52);
  for (int s = Hearts; s <= Spades; ++s) {
    for (int r = Ace; r <= King; ++r) {
      Card c(static_cast<Rank>(r), static_cast<Suit>(s));
      deck.push_back(c);
    }
  }
  print(deck);


#endif

  return 0;
}

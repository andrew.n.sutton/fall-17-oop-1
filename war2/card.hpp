// (c) 2017 Andrew Sutton, PhD
// All rights reserved

// Guarantee that this file is included exactly
// one time in any translation unit.
#pragma once

#include <iosfwd>
#include <utility>

enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King
};

enum Suit 
{
  Spades,
  Clubs,
  Hearts,
  Diamonds,
};

/// Represents the value of a (suited) playing card.
class Card
{
public:
  Card() = default;

  Card(Rank r, Suit s)
    : rank(r), suit(s)
  { }

  // Returns the rank of the card.
  Rank get_rank() const { return this->rank; }

  // Returns the suit of the card.
  Suit get_suit() const { return suit; }

private:
  Rank rank;
  Suit suit;
};

// Equality comparison
bool operator==(Card a, Card b);
bool operator!=(Card a, Card b);

// Ordering
bool operator<(Card a, Card b);
bool operator>(Card a, Card b);
bool operator<=(Card a, Card b);
bool operator>=(Card a, Card b);


// Declare the operator here.
// Only need to include <iosfwd>
std::ostream& operator<<(std::ostream& os, Card c);
std::ostream& operator<<(std::ostream& os, Rank r);
std::ostream& operator<<(std::ostream& os, Suit r);



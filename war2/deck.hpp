// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#pragma once

#include "card.hpp"

#include <vector>

// Very pragmatic. Don't write a class if you don't have to.

using Deck = std::vector<Card>;

// Generate a standard deck.
Deck make_standard_deck();

// Combine a set of decks.
Deck make_deck(const Deck& d1, const Deck& d2);

// Shuffle a deck of cards.
void shuffle(Deck& deck);

// Print a deck of cards.
void print(const Deck &deck);



// // Very object-oriented. Write more code, but possibly create
// // a better abstraction. Can add constructors, member functions,
// // etc.

// struct Deck
// {
//   Deck(Deck d1, Deck d2);

//   void shuffle();

//   std::vector<Card> cards;
// };


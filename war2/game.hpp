// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#pragma once

//  - selection of cards (jokers?)
//  - number decks
//  - ace high/low
//  - number sacrificial cards
//  - negotiable sacrifice
//  - acquisition method

struct Options
{
  bool use_jokers = false;
  int num_decks = 1;
  bool ace_high = true;
  int num_sacrifice = 1;
  bool negotiable_sacrifice = true;
};

struct Game
{
  Game(Options opts);

  void step();
  void run();

  Options opts;
  Deck deck;
  Player p1;
  Player p2;
  Pile pile;
};

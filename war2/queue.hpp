// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#pragma once

#include "Card.hpp"

#include <queue>

using CardQueue = std::queue<Card>;

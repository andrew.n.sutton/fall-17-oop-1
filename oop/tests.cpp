
#include "card.hpp"

#include <iostream>
#include <random>


void 
print(Card* c)
{
  if (c->type == SuitedCard) {
    Suited* s = static_cast<Suited*>(c);
    std::cout << s->rank << '\n';
  }
  else if (c->type == Joker) {
    Joker* j = static_cast<Joker*>(j);
    std::cout << j->color << '\n';
  }
  else {
    // I don't know how to define this. It's
    // some kind of a logic error.
    throw std::logic_error("unknown card");
  }
}


int 
main() {
  std::random_device rng;
  std::minstd_rand prng(rng());


  Spade c1 {0, Ace};
  Spade c2 {1, Two};

  Joker j1{52, Black};
  Joker j2{53, Red};

  Deck d;
  d.push_back(&c1);
  d.push_back(&c2);
  d.push_back(&j1);
  d.push_back(&j2);
  std::shuffle(d.begin(), d.end(), prng);
}

// (c) 2017 Andrew Sutton, PhD
// All rights reserved

// Guarantee that this file is included exactly
// one time in any translation unit.
#pragma once

#include <cassert>
#include <iosfwd>
#include <vector>

enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King
};


enum Color
{
  Black,
  Red,
};

// Types of cards.
enum CardType {
  SuitedCard,
  JokerCard,
};

// Represents the set of all cards.
struct Card
{
  Card(int id, CardType t) 
    : id(id), type(t)
  { }

  CardType type;
  int id;
};

// Types of suits.
enum SuitType {
  SpadesSuit,
  ClubsSuit,
  HeartsSuit,
  DiamondsSuit,
};

// A suited card IS-A card.
//
// (Card is a superclass, Suited is a subclass).
struct Suited : Card // Suited is derived from Card. 
{
  Suited(int id, SuitType s, Rank r) 
    : Card(id, SuitedCard), // Explicitly call the base class constructor.
      suit(s),
      rank(r) 
  { }
  
  SuitType suit;
  Rank rank;
};

struct Spade : Suited
{
  Spade(int id, Rank r) 
    : Suited(id, SpadesSuit, r)
  { }
};

struct Club : Suited
{
  Club(int id, Rank r) 
    : Suited(id, ClubsSuit, r)
  { }
};

struct Heart : Suited
{
  Heart(int id, Rank r) 
    : Suited(id, HeartsSuit, r)
  { }
};

struct Diamond : Suited
{
  Diamond(int id, Rank r) 
    : Suited(id, DiamondsSuit, r)
  { }
};


// A joker (card) IS-A card.
struct Joker : Card 
{
  Joker(int id, Color c) 
    : Card(id, JokerCard), color(c) 
  { }

  Color color;
};


// A collection heterogeneous objects.
//
// NOT a heterogeneous container.
using Deck = std::vector<Card*>;

// Don't do this... Card& is not an object.
// std::vector<Card&> cards2;




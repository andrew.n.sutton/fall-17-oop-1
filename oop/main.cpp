// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#include "card.hpp"

#include <cassert>
#include <iostream>
#include <queue>
#include <stack>

int
main() {
  // An object has both a static type and dynamic type. The static type
  // of an object is determined by the type in its declaration. The dynamic
  // type of an object is determined at the time of creation.

  // s is declared to be an object. The static type of the object is
  // 'Suited'. The dynamic type is also 'Suited'.
  Suited s{0, Ace, Spades};
  
  // j is declared to be object. As above, the static and dynamic types of
  // the object are both Joker.
  Joker j{51, Red};

  // ref is declared to be a reference to a specific Card object, and bound 
  // to the object s. The static type of ref is Card (that's how the variable 
  // is  declared). The dynamic type of the object is Suited (that's how s was
  // created).
  Card& ref = s;

  // ptr is declared to be a pointer to any Card object. Here, the pointer
  // is initialized to the address of s. The static type of ptr is Card, and
  // the dynamic type is Suited.
  Card* ptr = &s;

  // ptr is assigned to the address of j. The static type remains the same;
  // it is Card (that's how the variable was declared). After assignment, the
  // dynamic type of the ptr is `Joker`.
  ptr = &j;

  // c1 is declared to be a card object whose value is s. Both the static and
  // dynamic type of c1 are Card.
  Card c1 = s;
  // This will copy the Card part of the s object into c1. The rank and
  // suit are not copied.
  //
  // This is called *object slicing*. It is almost always a bug.

  // The static type of an object determines how you use it (e.g., what 
  // members are accessible).



// -------------------------------------------------------------------------- //


  std::cout << d.cards.back().id << '\n';

}

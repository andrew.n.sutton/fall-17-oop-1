#pragma

#include <iostream>
#include <vector>

enum Rank
{
  Ace,  Two,   Three, Four, Five,
  Six,  Seven, Eight, Nine, Ten,
  Jack, Queen, King
};

enum Suit 
{
  Spades, Clubs, Hearts, Diamonds,
};

enum Color
{
  Black, Red,
};

// Represents the set of all cards.
struct Card
{
  Card(int id) 
    : id(id)
  { }

  // A virtual function makes the behavior vary, depending
  // on the dynamic type of this (as the pointer this).
  //
  // A polymorphic class has at least one virtual function.
  //
  // This is shorthand for everything in card2 (kind of).
  // virtual void print() const { 
  //   throw std::logic_error("???");
  // }

  // Makes the function "pure virtual". Can also be
  // called an "abstract method". Pure virtual methods
  // MUST be overridden in a derived class.
  //
  // An abstract (base) class has at least one pure virtual function.
  virtual void print() const = 0;

  // Returns an integer hash value for a card.
  virtual unsigned hash() const { return id; }

  int id;
};


// A suited card IS-A card.
//
// (Card is a superclass, Suited is a subclass).
struct Suited : Card // Suited is derived from Card. 
{
  Suited(Rank r, Suit s, int id) 
    : Card(id), // Explicitly call the base class constructor.
      suit(s),
      rank(r)
  { }

  // This overrides the behavior of Card's print function.
  // print is an overridden function (or overrider).
  //
  // You can explicitly use the override word to indicate
  // this property.
  void print() const override { 
    std::cout << id << ' ' << suit << ' ' << rank << '\n'; 
  }

  unsigned hash() const override {
    return id * 1000 + suit * 100 + rank;
  }

  Suit suit;
  Rank rank;
};

struct Special : Suited
{
  Special()
    : Suited(Ace, Spades, -1)
  { }

  void print() const override
  {
    std::cout << "special!\n";
  }
};

// A joker (card) IS-A card.
struct Joker : Card 
{
  Joker(Color c, int id) 
    : Card(id), color(c) 
  { }

  virtual void print() const { 
    std::cout << id << ' ' << color << '\n';
  }

  Color color;
};

// Derives from Card.
struct UnoCard : Card
{
  UnoCard()
    : Card(0)
  { }

  // NOTE: No override of print().
};

struct SuitedUnoCard : UnoCard
{
  void print() const override { }
};


// // A deck is a sequence of cards.
using Deck = std::vector<Card*>;


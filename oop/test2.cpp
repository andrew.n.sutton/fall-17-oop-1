
#include "card2.hpp"

#include <iostream>

int
main()
{
  Deck d {
    new Suited{Ace, Spades, 0},
    new Suited{Two, Spades, 1},
    // ...
    new Joker{Black, 52},
    new Joker{Red, 53},
  };

  d[0]->print();
  d[1]->print();
  d[2]->print();
  d[3]->print();
}

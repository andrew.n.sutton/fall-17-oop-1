#pragma

#include <iostream>
#include <vector>

enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King
};

enum Suit 
{
  Spades,
  Clubs,
  Hearts,
  Diamonds,
};

enum Color
{
  Black,
  Red,
};

// Represents the set of all cards.
struct Card
{
  Card(int id) 
    : id(id), print_f(nullptr)
  { }

  void print() const { 
    print_f(this);
  }

  // This is a pointer to a function.
  using print_fn = void (*)(const Card*);
  using fash_fn = unsigned (*)(const Card*);

  // This is a member variable that points to a function.
  //
  // This is C-style object oriented programming.
  print_fn print_f;
  hash_fn hash_f;

  int id;
};


// A suited card IS-A card.
//
// (Card is a superclass, Suited is a subclass).
struct Suited : Card // Suited is derived from Card. 
{
  Suited(Rank r, Suit s, int id) 
    : Card(id), // Explicitly call the base class constructor.
      suit(s),
      rank(r)
  {
    print_f = print_card;
  }

  static void print_card(const Card* c) { 
    const Suited* s = static_cast<const Suited*>(c);
    std::cout << s->id << ' ' << s->suit << ' ' << s->rank << '\n'; 
  }

  Suit suit;
  Rank rank;
};

// A joker (card) IS-A card.
struct Joker : Card 
{
  Joker(Color c, int id) 
    : Card(id), color(c) 
  {
    print_f = print_card;
  }

  static void print_card(const Card* c) { 
    const Joker* s = static_cast<const Joker*>(c);
    std::cout << s->id << ' ' << s->color << '\n';
  }

  Color color;
};

// // struct UnoCard : Card
// // {
// //   // uno stuff...
// // };


// // A deck is a sequence of cards.
using Deck = std::vector<Card*>;


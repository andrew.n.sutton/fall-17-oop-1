
#include "card3.hpp"

#include <iostream>

void print(const Deck& d) {
  for (const Card* c : d)
    c->print();
}

int
main()
{
  Deck d {
    new Suited{Ace, Spades, 0},
    new Suited{Two, Spades, 1},
    // ...
    new Joker{Black, 52},
    new Joker{Red, 53},
    new Special{},
    // new UnoCard{}
  };

  print(d);

  d[0]->print();
  d[1]->print();
  d[2]->print();
  d[3]->print();

  // Calls to virtual functions select the most overridden
  // function.
  //
  // Dynamic dispatch -- what happens when you call a virtual
  // function.
  //
  // Static dispatch -- 
  d[4]->print();

  d[5]->print();
}

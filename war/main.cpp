// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#include "card.hpp"

#include <iostream>
#include <vector>

#include <random>

std::random_device rng;
std::minstd_rand prng;

void print(const std::vector<Card> &deck) {
  int i = 1;
  for (Card c : deck) {
    std::cout << c << ' ';
    if (i % 13 == 0) {
      std::cout << '\n';
      i = 0;
    }
    ++i;
  }
  std::cout << '\n';
}


bool suit_greater(Card a, Card b)
{
  return a.get_suit() < b.get_suit();
}

int 
main()
{
  rng(); // Can be called as a function.
  prng(); // Can be called as a function.

  prng.seed(rng());
  
  // std::uniform_int_distribution<> d6(1, 6);
  // std::bernoulli_distribution coin(.8);

  // Initialize a deck of cards.
  std::vector<Card> deck;
  deck.reserve(52);
  for (int s = Hearts; s <= Spades; ++s) {
    for (int r = Ace; r <= King; ++r) {
      Card c(static_cast<Rank>(r), static_cast<Suit>(s));
      deck.push_back(c);
    }
  }
  print(deck);

  // Put cards in random order.
  std::shuffle(deck.begin(), deck.end(), prng);
  print(deck);  

  std::sort(deck.begin(), deck.end());
  print(deck);  


  // std::shuffle(deck.begin(), deck.end(), prng);
  // std::sort(deck.begin(), deck.end(), [](Card a, Card b) { // Lambda expression
  //   return a > b;
  // });
  // for (Card c : deck) 
  //   std::cout << c << ' ';
  // std::cout << '\n';

  // std::shuffle(deck.begin(), deck.end(), prng);
  // std::sort(deck.begin(), deck.end(), [](Card a, Card b) { // Lambda expression
  //   return a.get_suit() > b.get_suit();
  // });
  // for (Card c : deck) 
  //   std::cout << c << ' ';
  // std::cout << '\n';

  // std::shuffle(deck.begin(), deck.end(), prng);
  // std::sort(deck.begin(), deck.end(), suit_greater);
  // for (Card c : deck) 
  //   std::cout << c << ' ';
  // std::cout << '\n';


  // for (int i = 0; i < 100; ++i) {
  //   int n = d6(prng);
  //   bool b = coin(prng);
  //   std::cout << n << ' ' << b << '\n';
  // }


  // Rank r1 = Two;
  // Rank r2 = Four;
  // std::cout << (r1 == r2) << '\n';
  // std::cout << (r1 < r2) << '\n';

  // // Widening conversion. Ok.
  // int n = Jack;
  // std::cout << n << '\n';
  // std::cout << Jack << '\n';

  // Narrowing conversion. Not allowed.
  // Rank r3 = -1; // error: cannot initialize.

  Card c1 {Ace, Spades};
  Card c2 {Two, Spades};
  Card c3 {Three, Spades};

  Card c;

  // This is a declaration; it invokes the copy constructor
  // to make c4 an copy of c1.
  Card c4 = c1;

  // This is an assignment (expression); it invokes
  // the copy assignment operator.
  c2 = c4;

  // Range-based for loop.
  // for (Card c : deck) {
  //   // std::cout << c.get_rank() << ' ' << c.get_suit() << '\n';
  //   std::cout << c << '\n';
  // }

  // The loop above expands into this.
  // 'auto' deduces the type of a variable from its initializer.
  // for (auto iter = deck.begin(); iter != deck.end(); ++iter) {
  //   Card c = *iter;
  //   std::couht << ???;
  // }

  // c3.rank = Four;
  // c3.suit = Hearts;

  // std::vector<Card> cards(52);
  // int i = 0;
  // for (Rank r = Ace; r <= King; ++r) {
  //   for (Suit s = Hearts; s <= Spades; ++s)
  //     cards[i++] = Card {r, s};
  // }

  return 0;
}

// (c) 2017 Andrew Sutton, PhD
// All rights reserved

// Guarantee that this file is included exactly
// one time in any translation unit.
#pragma once

#include <iosfwd>
#include <utility>

// A card is a rank (1-13) and suit (0-3)
// using Card = std::pair<int, int>;


// Not a valid card.
// Card c{}; // 0, 0

// Goal: make programs as safe as possible using the type system.

enum Rank // Rank is an enumeration (type)
{
  Ace, // Called an "enumerator"
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King
};

enum Suit 
{
  Hearts,
  Diamonds,
  Clubs,
  Spades,
};

// A card (value) is a pair of a rank and suit.
//
// Information hiding -- prevent access to implementation details
// of a data type (i.e., data members).
class Card
{
public:
  // This may not be a good default constructor.
  // It creates an object that is not an actual card.
  // Card() : rank(), suit() { }
  
  // Better.
  // Creates an object that is definitely not a card, but
  // you should just know that.
  // Card() { }

  // Best.
  Card() = default;

  // Construct a card with a rank and suit.
  Card(Rank r, Suit s)
    : rank(r), suit(s) // member initializer list
  { }

  // Don't write these if you don't have to.

  // // Copy constructor.
  // Card(const Card& c)
  //   : rank(c.rank), suit(c.suit)
  // { }

  // // Copy assignment operator.
  // Card& operator=(const Card& c)
  // {
  //   rank = c.rank;
  //   suit = c.suit;
  //   return *this;
  // }


  // Accessor functions.
  // Observer functions.

  // Returns the rank of the card.
  Rank get_rank() const { return this->rank; }

  // Returns the suit of the card.
  Suit get_suit() const { return suit; }


  // Mutator functions.
  // Modifier functions.
  //
  // Don't provide this unless you REALLY need to.

  // // Sets the rank of this card.
  // void set_rank(Rank r) { rank = r; }

  // // Sets the suit of this card.
  // void set_suit(Suit s) { suit = s; }

private:
  Rank rank;
  Suit suit;
};

// Equality comparison
bool operator==(Card a, Card b);
bool operator!=(Card a, Card b);

// Ordering
bool operator<(Card a, Card b);
bool operator>(Card a, Card b);
bool operator<=(Card a, Card b);
bool operator>=(Card a, Card b);


// Declare the operator here.
// Only need to include <iosfwd>
std::ostream& operator<<(std::ostream& os, Card c);
std::ostream& operator<<(std::ostream& os, Rank r);
std::ostream& operator<<(std::ostream& os, Suit r);



// Not real code.

// // Non-member function.
// //
// // This is essentially what Card::get_rank is.
// Rank get_rank(const Card* this) 
// {
//   return this->rank;
// }

// Rank set_rank(const Card* this, Rank r) 
// {
//   this->rank = r;
// }




#include "concept.hpp"

#include <cassert>


template<TotallyOrdered T>
T& min(T& a, T& b)
{
  return b < a ? b : a;
}

template<TotallyOrdered T>
const T& min(const T& a, const T& b)
{
  return b < a ? b : a;
}

int* min_pointer(int* p, int* q)
{
  return *q < *p ? q : p;
}


int* min_element(int* first, int* last)
{
  int* p = first;
  ++first; // Increment
  while (first != last) {
    if (*first < *p) // Access
      p = first;
    ++first;
  }
  return p;
}

struct node
{
  int value;
  node* next;
};

node* min_element(node* first, node* last)
{
  node* p = first;
  first = first->next; // Increment/successor
  while (first != last) { // Equality
    if (first->value < p->value) // Dereference/access
      p = first; // Assignment
    first = first->next;
  }
  return p;
}

template<typename I>
I generic_min_element(I first, I last)
{
  I min = first;
  advance(first);
  first = first->next; // Advance
  while (first != last) { // Equal
    if (get(first) < get(min)) // Get/read
      min = first;
    advance(first); // Advance
  }
  return p;
}

template<typename T>
T* advance(T*& ptr)
{
  ++ptr;
}

template<typename T>
T& get(T* ptr)
{
  return *ptr;
}

node* advance(node*& n)
{
  n = n->next;
}

int& get(node* n)
{
  return n->value;
}


template<typename I>
I actual_min_element(I first, I last)
{
  I p = first;
  ++first; // Increment the iterator
  while (first != last) {
    if (*first < *p) // Get values from iterators
      p = first; // Assign iterators
    ++first; // Increment the iterator
  }
  return p;
}

// An adaptor is a class that provides alternative names
// for various operations.
struct node_iterator
{
  node_iterator& operator++()
  {
    node = node->next;
  }

  int& operator*() 
  { 
    return node->value; 
  }

  bool operator==(node_iterator i) const
  {
    return node == i.node;
  }

  bool operator!=(node_iterator i) const
  {
    return node != i.node;
  }

  // Wraps an object of the underlying or adapted type.
  node* node;
};

// The recursive form of the loop.
// int* min_element(int* first, int* last)
// {
//   if (first == last)
//     return last;
//   ++first;
//   return min_element(first, last);
// }


int main()
{
  int a[] = {1, 3, 5, 0, 2, 4};
  int* p = min_element(a, a + 6);
  assert(p == &a[3]);
}


template<typename I>
void selection_sort(I first, I last)
{
  while (first != last) {
    swap(min_element(first, last), last);
    ++first;
  }
}



// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#include <iostream>

// function min(a, b) {
//   if (a < b)
//     return a;
//   else
//     return b;
// }

template<typename T>
T my_min(T a, T b)
{
  if (a < b)
    return a;
  else
    return b;
}

struct color
{
  float r, g, b;
};

int main()
{
  std::cout << my_min(0, 1) << '\n';
  std::cout << my_min(3.14, 2.82) << '\n';

  color red{1, 0, 0};
  color blue{0, 0, 1};
  
  // Can explicitly specify template arguments for
  // generic algorithms.
  my_min<color>(red, blue);

  // Let the compiler figure it out. This is called
  // template argument deduction.
  my_min(red, blue);
}
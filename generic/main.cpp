// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#include <vector>
#include <iostream>

// struct pair_int {
//   int first;
//   int second;
// };

// struct pair_float {
//   float first;
//   float second;
// };

// struct pair_string {
//   std::string first;
//   std::string second;
// };

// struct pair_int_bool {
//   int first;
//   bool second;
// };



template< // Indicates that pair is a template.
    typename T, // This is a template (type) parameter
    typename U> // This is also a type parameter
struct pair {
  T first;
  U second;
};

// Explicit instantiation. This just tells the compiler to
// instantiate the template.
//
// This doesn't declare a variable, just instantiates a type.
template struct pair<int, bool>;


#if 0
// This would be called the *primary template*.
//
// Primary templates are allowed to default template
// arguments.
template<typename T, typename A = std::allocator<T>>
class vector { ... };

// This is called an explicit specialization. When you try
// to use vector<bool>, you get this class instead of normal
// specialization of vector<T>.
template<>
struct vector<bool> { ... };

// Implicitly instantiating a primary template gives you
// an (implicit) explicit specialization.
// template<>
// struct vector<int> { ... };

// This is a partial specialization -- it's also a template.
// Whenever we use e.g., vector<int*>, we'll get this.
template<typename T>
class vector<T*> { ... };

#endif

void f(int a, int b);

int main() {
  // pair_int_int pii;
  // pair_int_float pif;

  pair< // Pair names a template.
      int, // Int is a template argument
      float>  // Float is a template argument.
    p1;

  // Instantiates a new version of pair (see below).
  // Implicitly instantiated.
  pair<bool, int> p2;

  // pair_int_bool p3;


  std::vector<int> v {1, 2, 3};
  for (auto& x : v) {
    std::cout << &x << '\n';
  }

  std::vector<bool> vb {1, 0 , 1};
  // for (auto& x : vb) {
  //   std::cout << &x << '\n';
  // }

  // This causes the template pair to be
  // *instantiated* using the template arguments
  // int and float. (see below).
}

// Instantiating pair<int, float> yields a new 
// class that looks (approximately) like called
// a template specialization. That looks about
// like this:
// template<>
// struct pair<int, float> {
//   int first;
//   float second;
// };

// The pair<bool, int> result in this specialization:
// template<>
// struct pair<bool, int> {
//   bool first;
//   float second;
// };







template<typename T>
  requires TotallyOrdered<T>()
T min(T a, T b)
{
  // return a < b ? a : b;
  return a + b;
}

struct Color { };

int main()
{
  min(3, 4);

  static_assert(TotallyOrdered<int>());
  static_assert(TotallyOrdered<Color>());

  Color r;
  Color b;
  min(r, b);
}

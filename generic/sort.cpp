

template<typename T>
concept bool Regular()
{
  return EqualityComparable<T>() && Copyable<T>();
}

template<typename I>
concept bool ForwardIterator()
{
  return Regular<T>() && requires (I i, I j) {
    { ++i } -> I&;
    { *i } -> auto&;
  };
}

///  This concept *refines* ForwardIterator.
template<typename I>
concept bool BidirectionalIterator()
{
  return ForwardIterator<I>() && requires (I i, I j) {
    { --i } -> I&;
  };
}

template<typename I>
concept bool RandomAccessIterator()
{
  return BidirectionalIterator<I>() && requires(I i, I j, int n) {
    { i += n } -> I&;
    { i -= n } -> I&;
    { i - j } -> int;
  };
}




template<ForwardIterator I>
I min_element(I first, I last)
{
  I p = first;
  ++first; // Advance (forward)
  while (first != last) {
    if (*first < *p) // Get (a reference)
      p = first;
    ++first; // Advance
  }
  return p;
}


// ---------------------------------------------

template<ForwardIterator I>
void advance(I& iter, int n)
{
  while (n != 0) {
    ++iter;
    --n;
  }
}

template<BidirectionalIterator I>
void advance(I& iter, int n)
{
  if (n > 0)
    while (n != 0) { ++iter; --n; }
  else if (n < 0)
    while (n != 0) { --iter; ++n; }
}

template<RandomAccessIterator I>
void advance(I& iter, int n)
{
  iter += n;
}






#include "concept.hpp"

template<ForwardIterator I, typename T>
I find(I first, I last, const T& value)
{
  while (first != last) {
    if (*first == value)
      return first;
    ++first;
  }
  return last;
}

template<typename F, typename... Args>
concept bool Funtion()
{
  return requires (F f, Args... args) {
    { f(args...) };
  };
}

// template<typename F, typename... Args>
// concept bool Predicate()
// {
//   return requires (F f, Args... args) {
//     { f(args...) } -> bool;
//   };
// }

template<typename T>
concept bool Predicate()
{
  return true;
}


template<ForwardIterator I, typename P>
  requires Predicate<P, int>()
I find_if(I first, I last, P pred)
{
  while (first != last) {
    if (pred(*first))
      return first;
    ++first;
  }
  return last;
}


// strict weak order
template<RandomAccessIterator I, Relation R>
void sort(I first, I last, R cmp) 
{
  std::sort(first, last, cmp);
}



bool
is_negative(int n)
{
  return n < 0;
}


int main()
{
  int a[] = {1, 2, 4, -1, 5, 6, 8};
  auto i = find_if(a, a + 7, is_negative);
};





template<typename T>
concept bool Sized() {
  return requires(T t) {
    { size(t) } -> int;
    requires size(t) ==
  };
}


#pragma once

template<typename T>
concept bool EqualityComparable() {
  return requires (T a, T b) {
    // axiom: ensures the regular functions are in
    // fact regular functions.
    { a == b } -> bool;

    // axiom: a != b <-> !(a == b)
    { a != b } -> bool;
  };
}

template<typename T>
concept bool TotallyOrdered() {
  return EqualityComparable<T>() && requires(T a, T b) {
    // axioms: One of the following must hold:
    //  a < b
    //  b < a
    //  a == b
    { a < b } -> bool;
    
    // axiom: a > b <-> b < a
    { a > b } -> bool;
    
    // axiom: a <= b <-> !(b < a)
    { a <= b } -> bool;
    
    // axiom: a <= b <-> !(a < b)
    { a >= b } -> bool;
  };
}

template<typename T>
concept bool Copyable() {
  return true;
}

template<typename T>
concept bool Regular()
{
  return EqualityComparable<T>() && Copyable<T>();
}

template<typename I>
concept bool ForwardIterator()
{
  return Regular<I>() && requires (I i, I j) {
    { ++i } -> I&;
    { *i } -> auto&;
  };
}

///  This concept *refines* ForwardIterator.
template<typename I>
concept bool BidirectionalIterator()
{
  return ForwardIterator<I>() && requires (I i, I j) {
    { --i } -> I&;
  };
}

template<typename I>
concept bool RandomAccessIterator()
{
  return BidirectionalIterator<I>() && requires(I i, I j, int n) {
    { i += n } -> I&;
    { i -= n } -> I&;
    { i - j } -> int;
  };
}
